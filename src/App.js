import React ,{Component} from 'react';
import  CourseForm from './course-form';
import CourseList from './course-list';
import NoItem from './no-item'
class App extends Component {
  state = {
    courses:[
    ],
    current:'',
  }


  updateCourse = (e) => {

    this.setState({
      current:e.target.value
    });
  }

  addCourse = (e) =>{
    e.preventDefault();
    const {courses} = this.state;
    const {current} = this.state;
    let {item} = this.state;

    if(current){
   
      courses.push({name:current});
    }
    this.setState({courses,current:''})
  }


  deleteCourse = (index) => {
      const courses = this.state.courses; 
      courses.splice(index,1);
      this.setState(courses)
  }


  editCourse = (index, value) => {
    const {courses} = this.state;
    const course = courses[index];
    course['name'] = value;
    this.setState({courses})
  }

 checkCourses = () => {
  const courses = this.state.courses;
    let courseLength = courses.length;
    if(!courseLength) {
      return <NoItem/>
    }
  }
 

  render(){
    
    const courses = this.state.courses;
 
     const courseItem = courses.map( (item,index) => {
       if(item){
      return <CourseList details={item} key={index} index={index} deleteCourse={this.deleteCourse} editCourse={this.editCourse}/>
       } else{
         return <NoItem/>
       }
   } )

   
    return (
      <section className="App">
        <h2>Add Course</h2>

          <CourseForm updateCourse={this.updateCourse}  current={this.state.current} addCourse={this.addCourse}/>
          <ul>
            {courseItem}
          </ul>
          {this.checkCourses()}
       
      </section>
    );
  }
}

export default App;
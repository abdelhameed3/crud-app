import React from 'react';


function CourseForm(props) {
  return (
    <form onSubmit={props.addCourse} className="addCourse">
        <input type="text" value={props.current} onChange={props.updateCourse}/>
        <button>Add Course</button>
    </form>
  );
}

export default CourseForm;

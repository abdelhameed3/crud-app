import React ,{Component,Fragment} from 'react';

class CourseList extends Component {
    
    state= {
        isEdit: false
    }

    renderCourse= () => {
        return(
            <li>
                <span>{this.props.details.name}</span>
                <button onClick={() => this.toggleState()}>Edit Course</button>
                <button onClick={() => this.props.deleteCourse(this.props.index)}>Delete Course</button>

            </li>
        )
    }
    renderUpdateForm= () => {
        return(
            <form onSubmit={this.updateForm} className="editCourse">
                <input type="text" defaultValue={this.props.details.name} ref={(v) => {this.input = v}}/>
                <button>Update Course</button>
            </form>
        )
    }
    toggleState = () => {
        let {isEdit} = this.state;
        isEdit = !isEdit;
        this.setState({isEdit})
    }
   
    updateForm = (e) => {
        e.preventDefault();
        this.props.editCourse(this.props.index,this.input.value);
        this.toggleState();
    }
    render(){
        return(
            <Fragment>
              {this.state.isEdit ? this.renderUpdateForm() : this.renderCourse() }
            </Fragment>
        )
    }
}

export default CourseList;